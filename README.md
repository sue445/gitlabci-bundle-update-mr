# Gitlabci::Bundle::Update::Mr
[![Gem Version](https://badge.fury.io/rb/gitlabci-bundle-update-mr.svg)](https://badge.fury.io/rb/gitlabci-bundle-update-mr)
[![pipeline status](https://gitlab.com/sue445/gitlabci-bundle-update-mr/badges/master/pipeline.svg)](https://gitlab.com/sue445/gitlabci-bundle-update-mr/commits/master)
[![coverage report](https://gitlab.com/sue445/gitlabci-bundle-update-mr/badges/master/coverage.svg)](https://gitlab.com/sue445/gitlabci-bundle-update-mr/commits/master)

Create MergeRequest of bundle update in GitLab CI

_GITLAB is a trademark of GitLab Inc. in the United States and other countries and regions_

![gitlabci-bundle-update-mr](img/gitlabci-bundle-update-mr.png)

This is inspired by [circleci-bundle-update-pr](https://github.com/masutaka/circleci-bundle-update-pr)

<details>
  <summary>TOC</summary>

[[_TOC_]]

</details>

## Requirements
* GitLab v9.0.0+
* GitLab API v4
* git

## Installation
```bash
gem install gitlabci-bundle-update-mr
```

## Prerequisites
The application on which you want to run continuous bundle update must be configured to be built on GitLab CI.

## Setup
### Get GitHub personal access token
1. Go to [your account's settings page](https://github.com/settings/tokens/new?description=gitlabci-bundle-update-mr%20token)
2. Generate a personal access token with `repo` scope

![github_access_token](img/github_access_token.png)

### Get GitLab personal access token
1. Go to `https://YOUR-GITLAB/profile/personal_access_tokens`
    * e.g. https://gitlab.com/profile/personal_access_tokens
2. Generate a personal access token with `api` scope

![gitlab_access_token](img/gitlab_access_token.png)

### Save CI environment variables 
1. Go to `https://YOUR-GITLAB/YOUR-NAME/REPO-NAME/settings/ci_cd`
2. Save environment variables
    * `GITLAB_API_ENDPOINT` : GitLab API endpoint (e.g. `https://gitlab.com/api/v4`)
    * `GITLAB_API_PRIVATE_TOKEN` : GitLab personal access token
    * `OCTOKIT_ACCESS_TOKEN` : GitHub personal access token

![gitlab_environment_variables](img/gitlab_environment_variables.png)

## Usage
### Step 1-A. Plain
Configure your `.gitlab-ci.yml` to run `gitlabci-bundle-update-mr`, for example:

```yml
stages:
  - build

continuous_bundle_update:
  stage: build

  image: ruby

  cache:
    key: "$CI_BUILD_NAME"
    paths:
      - vendor/bundle/

  before_script:
    # Set timezone to Asia/Tokyo
    - cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

  script:
    - bundle install --path vendor/bundle
    - bundle clean
    - gem install --no-doc gitlabci-bundle-update-mr
    - gitlabci-bundle-update-mr --user="GitLab CI" --email="gitlabci@example.com" --labels="bundle update"

  only:
    - schedules
```

### Step 1-B. Using `include`
`include` is available in one of the following versions.

* GitLab Core 11.4+
* GitLab Premium 10.5+
* Starter, Premium and Ultimate 10.6+

c.f. https://docs.gitlab.com/ce/ci/yaml/README.html#include

example

```yml
include:
  - remote: "https://gitlab.com/sue445/gitlabci-bundle-update-mr/raw/master/gitlabci-templates/continuous_bundle_update.yml"

continuous_bundle_update:
  stage: build

  variables:
    # override variables (followings are defaults)
    CACHE_VERSION: "v1"
    GIT_EMAIL:     "gitlabci@example.com"
    GIT_USER:      "GitLab CI"
    LABELS:        "bundle update"
    ASSIGNEES:     "" # If you want to set up multiple assignees, please separate them by commas (e.g. "foo, bar, baz")
    OPTIONS:       ""
    GEM_VERSION:   "" # default: install latest version of gitlabci-bundle-update-mr

  before_script:
    # Set timezone to Asia/Tokyo
    - cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
```

### Step 2. Register scheduler
And register scheduler to `https://YOUR-GITLAB/YOUR-NAME/REPO-NAME/pipeline_schedules`

![gitlab_scheduler](img/gitlab_scheduler.png)

## CLI command references
```bash
$ gitlabci-bundle-update-mr --help
Usage: gitlabci-bundle-update-mr [options]
        --email EMAIL                git email address (default. `git config user.email`)
        --user USER                  git username (default. `git config user.name`)
    -d, --duplicate                  Make MR even if it has already existed (default. false)
    -l, --labels 'In Review, Update' Add labels to the MR
        --update-bundled-with        Whether to update `BUNDLED WITH` section in Gemfie.lock (default. false)
        --merge-when-pipeline-succeeds
                                     Whether to set 'Merge when pipeline succeeds' (default. false)
        --assignees 'foo, bar'       Add assignees to the MR
```

## ProTip
### Randomly assign one person to MergeRequest
```yml
include:
  - remote: "https://gitlab.com/sue445/gitlabci-bundle-update-mr/raw/master/gitlabci-templates/continuous_bundle_update.yml"

continuous_bundle_update:
  stage: build

  before_script:
    - ASSIGNEES=$(ruby -e 'print %w[foo bar baz].sample')
```

### Assign one person to MR on a weekly basis
```yml
include:
  - remote: "https://gitlab.com/sue445/gitlabci-bundle-update-mr/raw/master/gitlabci-templates/continuous_bundle_update.yml"

continuous_bundle_update:
  stage: build

  before_script:
    - ASSIGNEES=$(ruby -rtime -e 'a=%w[foo bar baz]; print a[Time.now.to_date.cweek% a.size]')
```

It will be assigned weekly in the following order.

`foo` -> `bar` -> `baz` -> `foo` -> `bar` -> `baz` -> ...

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/sue445/gitlabci-bundle-update-mr.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
