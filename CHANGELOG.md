## Unreleased
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v5.0.2...master)

## v5.0.2
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v5.0.1...v5.0.2)

* Remove workaround for error in Ruby 3.4+ [!354](https://gitlab.com/sue445/gitlabci-bundle-update-mr/-/merge_requests/354) *@sue445*

## v5.0.1
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v5.0.0...v5.0.1)

* Fix error in Ruby 3.4+ [!346](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/346) *@sue445*

## v5.0.0
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v4.0.0...v5.0.0)

* Drop support ruby < 3.1.2 [!310](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/310) *@sue445*

## v4.0.0
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v3.0.0...v4.0.0)

* Drop support ruby < 3.0 [!294](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/294) *@sue445*

## v3.0.0
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v2.0.1...v3.0.0)

* Drop support ruby < 2.7 [!262](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/262) *@sue445*

## v2.0.1
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v2.0.0...v2.0.1)

* Auto retry when`Gitlab::Error::Unprocessable` is returned [!224](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/224) *@sue445*
* Add `$GEM_VERSION` to template [!223](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/223) *@sue445*

## v2.0.0
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v1.1.3...v2.0.0)

### Breaking changes :warning:
* Drop support ruby < 2.6 [!216](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/216) *@sue445*

## v1.1.3
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v1.1.2...v1.1.3)

* Enable MFA requirement for gem releasing [!181](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/181) *@sue445*

## v1.1.2
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v1.1.1...v1.1.2)

* Allow auto retry with `Gitlab::Error::NotAcceptable` [!134](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/134) *@sue445*

## v1.1.1
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v1.1.0...v1.1.1)

* Add auto retry when `merge when pipeline succeeds` is failed [!128](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/128) *@sue445*
* [Resolved] `open': can't modify frozen Hash: {} (FrozenError) [!131](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/131) *@sue445*

## v1.1.0
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v1.0.0...v1.1.0)

* Support assignee [!113](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/113) *@sue445*

## v1.0.0
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v0.3.0...v1.0.0)

### Breaking changes :warning:
* Drop support ruby < 2.5 [!87](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/87) *@sue445*

## v0.3.0
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v0.2.2...v0.3.0)

* Support "Merge when pipeline succeeds" [!41](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/41) *@sue445*

## v0.2.2
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v0.2.1...v0.2.2)

### Bugfix
* Bugfix. doesn't work `--update-bundled-with` option [!30](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/30) *@sue445*

## v0.2.1
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v0.2.0...v0.2.1)

### Others
* Add gitlabci-templates/continuous_bundle_update.yml [!27](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/27) *@sue445*
* bundle update at 2019-03-07 00:56:00 JST [!26](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/26) *@sue445*

## v0.2.0
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v0.1.1...v0.2.0)

### Features
* Don't update `BUNDLED WITH` section when ` budnle update` [!25](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/25) *@sue445*

### Others
* Add screenshot [!22](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/22) *@sue445*
* Refactor continuous_bundle_update job [!23](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/23) *@sue445*

## v0.1.1
[full changelog](https://gitlab.com/sue445/gitlabci-bundle-update-mr/compare/v0.1.0...v0.1.1)

### Refactorings
* Print MR url after created [!20](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/20) *@sue445*
* Search only bundle update MR [!21](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/21) *@sue445*

### Others
* Fixed unstable bundle update [!16](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/16) *@sue445*
* Setup simplecov [!17](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/17) *@sue445*
* Setup yard [!18](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/18) *@sue445*
* Rewrite spec [!19](https://gitlab.com/sue445/gitlabci-bundle-update-mr/merge_requests/19) *@sue445*

## v0.1.0
* first release

*This Change Log was automatically generated by [gitlab_awesome_release](https://gitlab.com/sue445/gitlab_awesome_release)*
