module Gitlabci
  module Bundle
    module Update
      module Mr
        VERSION = "5.0.2".freeze
      end
    end
  end
end
