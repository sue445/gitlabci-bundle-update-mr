require "gitlabci/bundle/update/mr/version"
require "gitlabci/bundle/update/mr/client"
require "gitlab"
require "compare_linker"
require "bundler"
require "restore_bundled_with"
require "restore_bundled_with/cli"

module Gitlabci
  module Bundle
    module Update
      module Mr
        class Error < StandardError; end

        class MissingKeyError < Error; end

        class NotFoundUserError < Error; end

        def self.assert_env_keys
          %w[
            OCTOKIT_ACCESS_TOKEN
            GITLAB_API_ENDPOINT
            GITLAB_API_PRIVATE_TOKEN
            CI_PROJECT_PATH
            CI_COMMIT_REF_NAME
          ].each do |key|
            assert_env_key(key)
          end
        end

        def self.assert_env_key(key)
          raise MissingKeyError, "#{key} is required" if !ENV[key] || ENV[key].empty?
        end
        private_class_method :assert_env_key
      end
    end
  end
end
