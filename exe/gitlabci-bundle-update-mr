#!/usr/bin/env ruby

require "gitlabci/bundle/update/mr"
require "optparse"
opt = OptionParser.new
opt.version = Gitlabci::Bundle::Update::Mr::VERSION

author_email = `git config user.email`.strip
author_name = `git config user.name`.strip
allow_dup_mr = false
mr_labels = []
update_bundled_with = false
merge_when_pipeline_succeeds = false
assignees = []

opt.on("--email EMAIL", "git email address (default. `git config user.email`)") {|v| author_email = v }
opt.on("--user USER", "git username (default. `git config user.name`)") {|v| author_name = v }
opt.on("-d", "--duplicate", "Make MR even if it has already existed (default. false)") {|v| allow_dup_mr = v }
opt.on("-l", "--labels 'In Review, Update'", Array, "Add labels to the MR") {|v| mr_labels = v.map(&:strip) }
opt.on("--update-bundled-with", "Whether to update `BUNDLED WITH` section in Gemfie.lock (default. false)") {|v| update_bundled_with = v }
opt.on("--merge-when-pipeline-succeeds", "Whether to set 'Merge when pipeline succeeds' (default. false)") {|v| merge_when_pipeline_succeeds = v }
opt.on("--assignees 'foo, bar'", Array, "Add assignees to the MR") {|v| assignees = v.map(&:strip) }

opt.parse!(ARGV)

Gitlabci::Bundle::Update::Mr.assert_env_keys

client = Gitlabci::Bundle::Update::Mr::Client.new(
  gitlab_api_endpoint:      ENV["GITLAB_API_ENDPOINT"],
  gitlab_api_private_token: ENV["GITLAB_API_PRIVATE_TOKEN"],
  project_name:             ENV["CI_PROJECT_PATH"],
  branch:                   ENV["CI_COMMIT_REF_NAME"],
  author_email:,
  author_name:,
)

client.perform(
  allow_dup_mr:,
  mr_labels:,
  update_bundled_with:,
  merge_when_pipeline_succeeds:,
  assignees:,
)
