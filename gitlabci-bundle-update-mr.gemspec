lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "gitlabci/bundle/update/mr/version"

Gem::Specification.new do |spec|
  spec.name          = "gitlabci-bundle-update-mr"
  spec.version       = Gitlabci::Bundle::Update::Mr::VERSION
  spec.authors       = ["sue445"]
  spec.email         = ["sue445@sue445.net"]

  spec.summary       = "Create MergeRequest of bundle update in GitLab CI"
  spec.description   = "Create MergeRequest of bundle update in GitLab CI"
  spec.homepage      = "https://gitlab.com/sue445/gitlabci-bundle-update-mr"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = spec.homepage
    spec.metadata["changelog_uri"] = "https://gitlab.com/sue445/gitlabci-bundle-update-mr/blob/master/CHANGELOG.md"
    spec.metadata["rubygems_mfa_required"] = "true"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
          "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject {|f| f.match(%r{^(test|spec|features|img)/}) }
  end

  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) {|f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.required_ruby_version = ">= 3.1.2"

  spec.add_dependency "bundler"
  spec.add_dependency "compare_linker", ">= 1.4.11"
  spec.add_dependency "git", "1.7.0"
  spec.add_dependency "gitlab", ">= 4.19.0"
  spec.add_dependency "restore_bundled_with"

  spec.add_development_dependency "dotenv"
  spec.add_development_dependency "gitlab_awesome_release"
  spec.add_development_dependency "onkcop", ">= 1.0.0.0"
  spec.add_development_dependency "rake", ">= 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "rspec-its"
  spec.add_development_dependency "rspec-temp_dir"
  spec.add_development_dependency "rubocop", ">= 1.29.0"
  spec.add_development_dependency "rubocop-ast", ">= 1.18.0"
  spec.add_development_dependency "rubocop_auto_corrector", ">= 0.4.4"
  spec.add_development_dependency "rubocop-performance", ">= 1.14.0"
  spec.add_development_dependency "rubocop-rspec", ">= 2.21.0"
  spec.add_development_dependency "simplecov"
  spec.add_development_dependency "simplecov-cobertura"
  spec.add_development_dependency "webmock"
  spec.add_development_dependency "yard"
end
