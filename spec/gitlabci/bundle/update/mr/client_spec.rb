RSpec.describe Gitlabci::Bundle::Update::Mr::Client do
  let(:client) do
    Gitlabci::Bundle::Update::Mr::Client.new(
      gitlab_api_endpoint:      "https://gitlab.example.com/api/v4",
      gitlab_api_private_token: "xxxxxx",
      project_name:             "sue445/example",
      branch:                   "master",
      author_email:             "dummy@example.com",
      author_name:              "dummy",
    )
  end

  def system!(command)
    # NOTE: system(exception: true) requires Ruby 2.6+
    ret = system(command)
    raise "`#{command}` is failed" unless ret
  end

  describe "#perform" do
    subject(:perform) do
      Bundler.with_original_env do
        client.perform(
          allow_dup_mr:                 false,
          mr_labels:                    [],
          update_bundled_with:,
          merge_when_pipeline_succeeds:,
          assignees:                    %w[john_smith],
        )
      end
    end

    include_context "within temp dir"

    before do
      FileUtils.cp("#{spec_dir}/fixtures/Gemfile", ".")
      FileUtils.cp("#{spec_dir}/fixtures/Gemfile.lock", ".")
      system!("git init")
      system!("git config user.email 'gitlabci@example.com'")
      system!("git config user.name 'GitLab CI'")
      system!("git add .")
      system!("git commit -am 'Initial commit'")

      stub_request(:post, "https://gitlab.example.com/api/v4/projects/sue445%2Fexample/repository/commits").
        to_return(status: 200, body: fixture("api/create_commit.json"))
      stub_request(:post, "https://gitlab.example.com/api/v4/projects/sue445%2Fexample/merge_requests").
        to_return(status: 200, body: fixture("api/create_merge_request.json"))
      stub_request(:get, "https://gitlab.example.com/api/v4/projects/sue445%2Fexample/merge_requests?search=bundle%20update%20at&state=opened&target_branch=master").
        to_return(status: 200, body: fixture("api/merge_requests.json"))
      stub_request(:get, "https://gitlab.example.com/api/v4/users?username=john_smith").
        to_return(status: 200, body: fixture("api/list_users_with_john_smith.json"))

      allow(client).to receive(:merge_request_description) do
        <<~MARKDOWN
          **Updated RubyGems:**

          * [ ] [rspec-expectations](https://github.com/rspec/rspec-expectations): [`3.8.0...3.8.2`](https://github.com/rspec/rspec-expectations/compare/v3.8.0...v3.8.2)

          Powered by [gitlabci-bundle-update-mr](https://rubygems.org/gems/gitlabci-bundle-update-mr)
        MARKDOWN
      end
    end

    let(:update_bundled_with) { false }
    let(:merge_when_pipeline_succeeds) { false }

    it { expect { subject }.to output(%r{MR is created: http://gitlab\.example\.com/my-group/my-project/merge_requests/1}).to_stdout }

    context "When merge_when_pipeline_succeeds is disabled" do
      let(:merge_when_pipeline_succeeds) { false }

      it { expect { subject }.not_to output(/Set merge_when_pipeline_succeeds/).to_stdout }
    end

    context "When merge_when_pipeline_succeeds is enabled" do
      let(:merge_when_pipeline_succeeds) { true }

      before do
        stub_request(:put, "https://gitlab.example.com/api/v4/projects/sue445%2Fexample/merge_requests/1/merge").
          with(body: { merge_when_pipeline_succeeds: true, should_remove_source_branch: true }).
          to_return(status: 200, body: fixture("api/accept_merge_request.json"))
      end

      it { expect { subject }.to output(%r{Set merge_when_pipeline_succeeds to http://gitlab\.example\.com/my-group/my-project/merge_requests/1}).to_stdout }
    end

    describe "Gemfile.lock" do
      subject do
        perform
        File.read("Gemfile.lock")
      end

      context "update_bundled_with is false" do
        let(:update_bundled_with) { false }

        it { should_not include "BUNDLED WITH" }
      end

      context "update_bundled_with is true" do
        let(:update_bundled_with) { true }

        it { should include "BUNDLED WITH" }
      end
    end
  end

  describe "#find_by_username" do
    subject { client.find_by_username(user_name) }

    let(:user_name) { "john_smith" }

    before do
      stub_request(:get, "https://gitlab.example.com/api/v4/users?username=john_smith").
        to_return(status: 200, body: fixture("api/list_users_with_john_smith.json"))
    end

    its(:id) { should eq 1 }
  end

  describe "#find_by_username!" do
    subject { client.find_by_username!(user_name) }

    let(:user_name) { "john_smith" }

    context "User is found" do
      before do
        stub_request(:get, "https://gitlab.example.com/api/v4/users?username=john_smith").
          to_return(status: 200, body: fixture("api/list_users_with_john_smith.json"))
      end

      its(:id) { should eq 1 }
    end

    context "User isn't found" do
      before do
        stub_request(:get, "https://gitlab.example.com/api/v4/users?username=john_smith").
          to_return(status: 200, body: "[]")
      end

      it { expect { subject }.to raise_error Gitlabci::Bundle::Update::Mr::NotFoundUserError }
    end
  end
end
