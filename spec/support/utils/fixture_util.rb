module FixtureUtil
  def fixture(file)
    spec_dir.join("fixtures", file).read
  end
end

RSpec.configure do |config|
  config.include FixtureUtil
end
